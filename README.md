# AWX Setup
* Create AWX credential of type "Ansible Tower" with a service account with appropriate
  privileges to add a survey_spec to a template.
* Create AWX credential of type "Vault" with the password for the ansible vault
  encrypted secrets file in the git repo.
* Create AWX credential of type "Source Control" with appropriate creds to clone
  repos from Bitbucket.
* Create a project pointing to the git repo.
* Create templates as specified below, all using the project above.

# Playbooks
There are three primary and one helper playbook available:

## top.yml
This is the starting point for creating a new VMware datastore.  It starts by parsing
the information given in the user survey form that is presented to the user when the AWX template
is launched.  It goes on to determine the name of the new lun based on the selected
datastore cluster and any previous numbered luns, incrementing it by one.

Once all the pieces of information are collected the playbooks to actually create the netapp
volume and lun are called, as well as a playbook to mount the new lun as a datastore.

## netapp_delete_lun.yml
This playbook will remove a netapp lun/volume as named by the user.

## awx_survey_spec.yml
This is a helper playbook that will be called from a scheduled AWX template, or from a ci/cd
pipeline based on the `datastore_map.yml` input.  This will create a AWX user survey containing
a custom dropdown list of all available datastore cluster and aggregate combinations.

## aggregate_summary.yml
This playbook will provide a summary of current aggregate usage to aid operator in selecting
the appropriate aggregate tu create luns in.

# AWX Templates
There are four AWX templates in this workflow.
## Aggregate_Summary
This template will call the `aggregate_summary.yml` playbook and pass it the
`vsphere_datastore_cluster_aggregate` variable that is set by the user survey dropdown
that is presented upon launch.

### Credentials required
* Credential of type Vault containing the ansible vault password to
  decrypt the secrets to access the netapp and VMware resources.

## Create_Datastore
This template will call the `top.yml` playbook and pass it the `lun_size` and
`vsphere_datastore_cluster_aggregate` variables that are set by the user survey dropdown
that is presented upon launch.

### Credentials required
* Credential of type Vault containing the ansible vault password to
  decrypt the secrets to access the netapp and VMware resources.


## Delete_Datastore
This template will call the `netapp_delete_lun.yml` playbook and pass it the `lun_name` and
`vsphere_datastore_cluster_aggregate` variables that are set by the user survey dropdown
that is presented upon launch.

### Credentials required
* Credential of type Vault containing the ansible vault password to
  decrypt the secrets to access the netapp and VMware resources.


## Update_Surveys
This template will call the `awx_survey_spec.yml` playbook that will populate the user
survey dropdown lists in the three previous templates with valid combinations of
datastore clusters and aggregates for the operator to choose from.

The `awx_survey_spec.yml` playbook interacts with the AWX server to build and attach
the user surveys to the templates.  As such it will need some additional credentials and
variables.  See the notes at the top of this document.

The variables are:
```
---
datastore_template_name: Create_Datastore
summary_template_name: Aggregate_Summary
delete_template_name: Delete_Datastore

tower_url: "https://awx-web-svc-inf-auto.apps.ocp.dev.invesco.net"
tower_username: "{{ lookup('env', 'TOWER_USERNAME') }}"
tower_password: "{{ lookup('env', 'TOWER_PASSWORD') }}"
```

The three `template_name` vars indicate the names of the templates that need a user survey attached
The `tower_url` vars is the location and creds of this AWX server.

### Credentials required
* Credential of type Vault containing the ansible vault password to
  decrypt the secrets to access the netapp and VMware resources.
* Credential of type Ansible Tower which contains the creds of an appropriately privileged AWX user.

# Datastore Map
All the information required to connect to and create netapp and VMware resources come from the master
file `datastore_map.yml`.  This file contains the hostnames, igroups datastore cluster names, etc. and
looks something like below, with one entry per datastore cluster:

```
dsclusters:
  - name: Lab_DS_Cluster
    ds: cdi_test
    igroup: LabEsx
    svm: ushoustor2003-Esx
    vsphere_hostname: ushouvc01vd.corp.amvescap.net
    vsphere_datacenter: HOU-Lab
    vsphere_cluster: HOU_Lab_01
    esxi_hostname: labesx100.ops.invesco.net
    netapp_hostname: ushoustor20.ops.invesco.net
    aggregates:
      - node01_SSD01
      - node02_SSD01
```

There is a helper function `makemap.awk` that can be used to create this list from a excel spreadsheet
that can be executed like so (see target in `Makefile`):

```
cat spreadsheet | awk -f makemap.awk
```

# TODO
* Move secrets to AWX credentials for simpler maintenance
