#!/usr/bin/python

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r'''
---
module: mapagg

description:

version_added: "1.0.0"

options:
    mapagg:
        description:
        required: true
        type:

author:
    - Lars Jonsson (lars.jonsson@cdillc.com)
'''

EXAMPLES = r'''
'''

RETURN = r'''
'''

from ansible.module_utils.basic import AnsibleModule


def run_module():
    module_args = dict(
        mapagg=dict(type='list', required=True)
    )

    result = dict(
        mapagg=[]
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    if module.check_mode:
        module.exit_json(**result)

    a = []
    for i in module.params['mapagg']:
        for j in i[1]:
            a = a + [i[0] + "|" + j]

    result['mapagg'] = a

    if module.params['mapagg']:
        result['changed'] = True

    if module.params['mapagg'] == 'fail me':
        module.fail_json(msg='You requested this to fail', **result)

    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()
