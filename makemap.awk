{
    print "  - name: " $1;
    print "    ds: " $1;
    print "    igroup: " $8;
    print "    svm: " $9;
    print "    vsphere_hostname: " $2;
    print "    vsphere_datacenter: " $4;
    print "    vsphere_cluster: " $5;
    print "    esxi_hostname: " $6;
    print "    netapp_hostname: " $7;
    print "    aggregates: ";
    print "      - foo";
    print "      - bar";
    print ""
}
