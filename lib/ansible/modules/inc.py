#!/usr/bin/python

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r'''
---
module: inc

description: Module to determine the highest numbered item
in a list of strings of the form ".*_[0-9]+"

version_added: "1.0.0"

options:
    list:
        description: List of strings
        required: true
        type: list(str)

author:
    - Lars Jonsson (lars.jonsson@cdillc.com)
'''

EXAMPLES = r'''
'''

RETURN = r'''
'''

from ansible.module_utils.basic import AnsibleModule


def run_module():
    module_args = dict(
        list=dict(type='list', required=True)
    )

    result = dict(
        last=[]
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    if module.check_mode:
        module.exit_json(**result)

    a = []
    for i in module.params['list']:
        a.append(int(i.split('_')[-1]))

    a.sort()

    if len(a) == 0:
        result['last'] = 0
    else:
        result['last'] = a[-1]

    if module.params['list']:
        result['changed'] = False

    if module.params['list'] == 'fail me':
        module.fail_json(msg='You requested this to fail', **result)

    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()
