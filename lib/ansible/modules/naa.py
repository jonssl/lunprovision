#!/usr/bin/python

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r'''
---
module: naa

short_description: Module to convert netapp lun serial number to naa id

version_added: "1.0.0"

description: Module to convert netapp lun serial number to naa id

options:
    serial:
        description: This is the netapp lun serial number
        required: true
        type: str

author:
    - Lars Jonsson (lars.jonsson@cdillc.com)
'''

EXAMPLES = r'''
'''

RETURN = r'''
'''

from ansible.module_utils.basic import AnsibleModule


def run_module():
    module_args = dict(
        serial=dict(type='str', required=True),
        vendor_prefix=dict(type='str', required=True),
    )

    result = dict(
        naa=''
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    if module.check_mode:
        module.exit_json(**result)

    result['naa'] = module.params['vendor_prefix'] + "".join([hex(ord(c))[2:] for c in module.params['serial']])

    if module.params['serial']:
        result['changed'] = True

    if module.params['serial'] == 'fail me':
        module.fail_json(msg='You requested this to fail', **result)

    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()
